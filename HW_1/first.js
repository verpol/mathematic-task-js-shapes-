function  Shape(color) {
	this.color = color;
	this.draw = function draw() {
		console.log(this.color);
	};
	this.getArea = function getArea() {
		console.log(0);
	}
}

function Line(color, startPoint, endPoint) {
	this.__proto__ = new Shape(color);
	this.startPoint = startPoint;
	this.endPoint = endPoint;
	this.getLength = function getLength() {
		console.log( Math.sqrt( Math.pow((this.endPoint[0] - this.startPoint[0]), 2) + Math.pow((this.endPoint[1] - this.startPoint[1]), 2) ) );
	}
}

function Rectangle(color, length, width) {
	this.__proto__ = new Shape(color);
	this.length = length;
	this.width = width;
	this.getArea = function () {
		console.log( this.length * this.width );
	}
}

function Circle(color, radius) {
	this.__proto__ = new Shape(color);
	this.radius = radius;
	this.getArea = function () {
		console.log( Math.PI * this.radius * this.radius );
	}
}

let line = new Line('#151aa5', [2, 15], [-13, -8]);
line.getLength();

let rectangle = new Rectangle('#108022', 5, 12);
rectangle.getArea();

let circle = new Circle('#ff1d17', 6);
circle.getArea();