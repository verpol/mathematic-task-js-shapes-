import {Line, Rectangle, Circle} from "./line_rectangle_circle";

let line = new Line('#151aa5', [2, 15], [-13, -8]);
line.getLength();

let rectangle = new Rectangle('#108022', 5, 12);
rectangle.getArea();

let circle = new Circle('#ff1d17', 6);
circle.getArea();