import Shape from "./shape";

export class Line extends Shape {
	constructor(color, startPoint, endPoint){
		super(color);
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}
	getLength() {
		console.log( Math.sqrt( Math.pow((this.endPoint[0] - this.startPoint[0]), 2) + Math.pow((this.endPoint[1] - this.startPoint[1]), 2) ) );
	}
}

export class Rectangle extends Shape {
	constructor (color, length, width) {
		super(color);
		this.length = length;
		this.width = width;
	}
	getArea() {
		console.log( this.length * this.width );
	}
}

export class Circle extends Shape {
	constructor(color, radius){
		super(color);
		this.radius = radius;
	}
	getArea() {
		console.log( Math.PI * this.radius * this.radius );
	}
}